<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckOutController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
})->middleware('auth')->name('home');

Route::get('/', [CustomerController::class, 'index'])->name('customer');
Route::get('list-product/{product}', [CustomerController::class, 'listProduct'])->name('listproduct.show');
Route::get('list-detail-product/{product}/{category}', [CustomerController::class, 'listDetailProduct'])->name('listdetailproduct.show');
Route::get('login', [AuthController::class, 'login'])->name('login');
Route::post('login', [AuthController::class, 'logon'])->name('logon');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('register', [UserController::class, 'register'])->name('register');
Route::get('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot.Password');
Route::get('email', [AuthController::class, 'checkMail'])->name('checkMail');
Route::put('Change-password', [AuthController::class, 'changePassword'])->name('change.Password');

Route::group(['prefix' => 'user', 'middleware' => ['auth', 'role:'.role_user]], function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('cart', [CartController::class, 'index'])->name('cart.index');
    Route::post('cart-store', [CartController::class, 'addcart'])->name('cart.store');
    Route::delete('cart-delete/{cart}', [CartController::class, 'destroy'])->name('cart.delete');
    Route::get('history', [HistoryController::class, 'index'])->name('history.index');
    Route::post('checkout', [CheckOutController::class, 'payment'])->name('payment');
    Route::get('payment', [CheckOutController::class, 'returnPayment'])->name('return.payment');
    Route::get('history/{history}', [HistoryController::class, 'detail'])->name('history.detail');
    Route::resource('transactions', TransactionController::class);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:'.role_admin]], function () {

    Route::get('add-user', [UserController::class, 'addUser'])->name('add.user');
    Route::get('restore/{restore}', [CategoryController::class, 'restore'])->name('category.restore');
    Route::resource('categories', CategoryController::class);
    Route::get('listSoftDelete', [CategoryController::class, 'listSoftDelete'])->name('list.categorysoftDelete');
    Route::delete('realdelete/{realdelete}', [CategoryController::class, 'realDelete'])->name('category.realDelete');
    Route::resource('users', UserController::class);
});
Route::group(['prefix' => 'shop', 'middleware' => ['auth', 'role:'.role_shop]], function () {
    Route::get('productSoftDelete', [ProductController::class, 'listSoftDelete'])->name('product.softDelete');
    Route::get('restore/{restore}', [ProductController::class, 'restore'])->name('product.restore');
    Route::delete('realdelete/{realdelete}', [ProductController::class, 'realDelete'])->name('product.realDelete');
    Route::get('order', [OrderController::class, 'index'])->name('order.index');
    Route::put('order/{order}', [OrderController::class, 'update'])->name('order.update');
    Route::post('product-discount', [ProductController::class, 'updateDiscount'])->name('product.updateDiscount');
    Route::resource('products', ProductController::class);
});
Route::resource('users', UserController::class)->only('store');
