<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    private $cart;
    public function __construct(Product $product, Cart $cart)
    {
        $this->cart = $cart;
    }
    public function index()
    {
        $carts = $this->cart->with('product')->get();
        return view('carts.index', compact('carts'));
    }
    public function addcart(Request $request)
    {
        $cart = $this->cart->whereProduct($request->product_id)->first();
        $soluong = 0;
        if ($cart != null) {
            $soluong =  $cart->soluongsp +  $request->soluongsp;
            $data = $this->cart->updateOrCreate(
                ['product_id' => $cart->product_id],
                [
                    'soluongsp' => $soluong,
                ]
            );
        } else {
            $data = $this->cart->create($request->all());
        }

        return response()->json(['data' => $data]);
    }
    public function destroy($id)
    {
        $this->cart->find($id)->delete();
        return response()->json(200);
    }
}
