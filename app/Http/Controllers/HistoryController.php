<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Transaction;
use Illuminate\Http\Request;


class HistoryController extends Controller
{
    private $history;
    private $transaction;
    public function __construct(History $history, Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->history = $history;
    }
    public function index()
    {   
        
        $histories = $this->history->listHistory(auth()->user()->id)->get();
        if(count($histories)>0){
            $this->transaction->status($histories);
        }
        return view('histories.index', compact('histories'));
    }
   
    public function detail($id)
    {
      
        $details = $this->history->whereHistory($id)->get();
        if( count($details)>0){
            $this->transaction->status($details);
        }
        return view('histories.detail', compact('details'));
    }
}
