<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Transaction;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $history;
    private $transaction;
    public function __construct(History $history, Transaction $transaction)
    {
        $this->history = $history;
        $this->transaction = $transaction;
    }
    public function index()
    {
        $orders = $this->history->with('transaction')->get();
        return view('products.order', compact('orders'));
    }
    public function update($id, Request $request)
    {
        $this->history->find($id)->update([
            'status' => status_off,
        ]);
        return redirect()->route('order.index');
    }
}
