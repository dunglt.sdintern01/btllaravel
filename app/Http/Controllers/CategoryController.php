<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;

use Illuminate\Http\Request;
use Psy\Readline\Hoa\FileLink;

class CategoryController extends Controller
{
    private $category;
    public function __construct(Category $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->all();
        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return view('categories.createorupdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if ($request->has('image_link')) {
            $file = $request->image_link;
            $ext = $request->image_link->extension();
            $file_name = time() . "-" . "category." . $ext;
            $file->move(public_path('uploads'), $file_name);
        }
        $request->merge(['image' => $file_name]);
        $this->category->create($request->only(['name', 'image']));
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.createorupdate')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {

        if ($request->hasfile('image_link')) {
            $file = $request->image_link;
            $ext = $request->image_link->extension();
            $file_name = time() . "-" . "category." . $ext;
            $file->move(public_path('uploads'), $file_name);
            unlink(public_path('uploads/'."$category->image"));
            $this->category->find($category->id)->update([
                'name' => $request->name,
                'image' => $file_name
            ]);
        } else {
            $this->category->find($category->id)->update([
                'name' => $request->name,
                'image' => $category->image
            ]);
        }
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->category->find($category->id)->delete(); 
        return redirect()->route('categories.index');
    }
    public function listSoftDelete()
    {
        $softCategories = $this->category->onlyTrashed()->get();
        return view('categories.list_softdelete')->with('softCategories', $softCategories);
    }
    public function restore($id)
    {
        $category = $this->category->onlyTrashed()->where('id', $id)->first();
        $category->restore();
        return redirect()->route('categories.index');
    }
    public function realDelete($id)
    {
        $user = $this->category->onlyTrashed()->where('id', $id)->first();
        unlink(public_path('uploads/'.$user->image));
        $user->ForceDelete();
        return redirect()->back()->with('success', 'Đã xóa user khỏi database ');
    }
}
