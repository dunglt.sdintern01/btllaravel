<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forgot_password extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'token',
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function whereForgotPassword($token){
        return $this->where('token',$token);
    }
}
