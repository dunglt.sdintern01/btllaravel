<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpParser\Node\Stmt\Static_;

class Product extends Model
{
    
    use HasFactory,SoftDeletes;
    const PAGIANION=10;
    protected $fillable=[
        'user_shop_id',
        'category_id',
        'name',
        'price',
        'discount',
        'image',
        'create',

    ];
    public function user(){
        return $this->belongsTo(User::class,'user_shop_id','id');
    }
    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function images(){
        return $this->hasMany(Image::class,'product_id','id');
    }
    public function carts(){
        return $this->hasMany(Cart::class,'product_id','id');
    }
    public function histories(){
        return $this->hasMany(History::class,'product_id','id');
    }
    public function whereProduct($id){   
        return $this->where('id',$id);
    }
    public function whereCategoryProduct($id){
        return $this->where('category_id','=',$id);
    }
    public function whereUserProduct($id){
        return $this->where('user_shop_id','=',$id);
    }
    public function scopeSearch($query){
        if($key=request()->key){
            $key=request()->key;
            $query=$query->where('name','LIKE','%'.$key.'%');
        }
        return $query;
    }
    
    public function productOrderBy(){
          return $this->take(5)->orderBy('discount','DESC')->get();
       
    }
}
