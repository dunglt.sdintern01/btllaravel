<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable=[
        'product_id',
        'image_link',
    ];
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function whereImage($id){
        return $this->where('product_id',$id);
    }

    public function smallImage($images,$new_product)
    {
        for ($i = 0; $i < count($images); $i++) 
        {
            $small_image = $images[$i];
            $ext = $small_image->extension();
            $small_image_name =substr(rand(0, 999999), 0, 8) . '-' . 'product-' . "$new_product->id" . '.' . $ext;
            $this->create(
                [
                    'product_id' => $new_product->id,
                    'image_link' => $small_image_name
                ]
            );
            $small_image->move(public_path('small_images'), $small_image_name);
        }
    }
    public function updateImage($smallimages,$request_images,$product)
    {
        foreach ($smallimages as $key1 => $smallimage) {
            foreach ($request_images as $key2 => $small_image) {
                if ($key1 == $key2) 
                {
                    $small = $small_image;
                    $ext = $small_image->extension();
                    $file_name = substr(rand(0, 999999), 0, 8) . '-' . 'product-' . "$product->id" . '.' . $ext;
                    $small->move(public_path('small_images'), $file_name);
                    $this->find($smallimage->id)->update([
                        'image_link' => $file_name,
                    ]);
                    unlink(public_path('small_images/' . $smallimage->image_link));
                }
            }
        }
    }

    public function deleteImage($images)
    {
        foreach($images as $image )
        { 
            unlink(public_path('small_images/'.$image->image_link));
            $this->find($image->id)->delete();
        }
    }
}
