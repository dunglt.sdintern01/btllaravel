<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;
    protected $fillable = [
        'transaction_id',
        'product_id',
        'price',
        'status',
        'name',
        'soluong',
    ];
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function whereHistory($id)
    {
        return $this->where('transaction_id', $id);
    }
    public function createHistory($transaction, $carts)
    {
        foreach ($carts as $cart) {

            $this->create(
                [
                    'transaction_id' => $transaction->id,
                    'product_id' => $cart->product_id,
                    'price' => $cart->product_price,
                    'name' => $cart->product_name,
                    'soluong' => $cart->soluongsp,
                ]
            );
        }
    }
    public function listHistory($user_id)
    {
        return $this->join('transactions', 'histories.transaction_id', '=', 'transactions.id')->select('transaction_id', 'transactions.status', 'total', 'user_id')->groupBy('transaction_id', 'transactions.status', 'total', 'user_id')->having('user_id', '=', $user_id);
    }
}
