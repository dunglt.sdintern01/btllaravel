<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    private $token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public function __construct($token)
    {
       $this->token=$token;
    //    $this->queue = "email";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Auths.email',[
            'token' => $this->token
        ]);
    }
}
