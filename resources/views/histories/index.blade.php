@extends('layouts.view_customer')
@section('css')
    <!-- Latest compiled and minified CSS -->
@endsection
@section('content')
    <section class="cart">
        <div class="container">
            <div class="cart-content row">

                <div class="cart-content-left">


                    <table>
                        <tr>

                            <th>Mã giao dịch</th>
                            <th>tình trạng</th>
                            <th>Tổng tiền hàng đã giảm giá</th>
                            <th></th>

                        </tr>


                        @foreach ($histories as $history)
                            <tr>
                                <td>{{ $history->transaction_id }}</td>
                                <td>
                                    @if ($history->status == status_on)
                                        {{ 'Đang xử lý' }}
                                    @else
                                        {{ 'Đã thanh toán' }}
                                    @endif
                                </td>
                                <td>{{ $history->total }}</td>
                                <td><a href="{{ route('history.detail', $history->transaction_id) }}">xem chi tiết</a>
                                </td>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
@endsection
@section('js')
@endsection
