@extends('layouts.view_customer')
@section('css')
    <!-- Latest compiled and minified CSS -->
@endsection
@section('content')
    <section class="cart">
        <div class="container">

            <div class="cart-content row">

                <div class="cart-content-left">
                    <table>
                        <tr>
                            <th>Tên sản phẩm</th>
                            <th>tình trạng</th>
                            <th>Số lượng</th>
                            <th>Tổng tiền hàng đã giảm giá</th>
                            <th></th>
                        </tr>

                        @foreach ($details as $detail)
                            <tr>
                                <td>{{ $detail->name }}</td>
                                <td>@if ($detail->status==status_on)
                                    {{'Chưa thanh toán'}}
                                @else
                                    {{'Đã thanh toán'}}
                                @endif
                                   </td>
                                <td> {{$detail->soluong}}</td>
                                <td>{{ $detail->price }}</td>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
@endsection
