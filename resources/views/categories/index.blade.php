@extends('layouts.view_home')
@section('content')
    <div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Danh sách loại sản phẩm</h3>
        </div>
        <div class="card">
            <div class="card-body">

                <div class="p-2 bd-highlight">
                    <a href="{{ route('categories.create') }}" class="btn btn-success">Add</a>
                    <a class="btn btn-secondary" href="{{ route('list.categorysoftDelete') }}">Restore</a>
                </div>

                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th>STT</th>
                            <th>Tên loại sản phẩm</th>
                            <th>Image</th>
                            <th style="width:20px;">Edit</th>
                            <th style="width:20px;">Delete</th>
                        </tr>
                    </thead>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $category->name }}</td>
                            <td><img style="height: 50px;width: 50px; " src="/uploads/{{ $category->image }}"></td>
                            <td style="width:20px;"><a class="btn btn-primary"
                                    href="{{ route('categories.edit', $category->id) }}">Edit</a></td>
                            <td style="width:20px;">
                                <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    <tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
