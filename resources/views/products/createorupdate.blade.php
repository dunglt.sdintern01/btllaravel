@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body" style="color: black">
        <div class="card-header">
            <h3 style="color: blue" class="card-title">Create product</h3>
        </div>
        <div class="card">

            <div class="card-body">
                <form
                    action=" @if (isset($product->id)) {{ route('products.update', $product->id) }}
                @else
                    {{ route('products.store') }} @endif "
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    @if (isset($product->id))
                        @method('PUT')
                    @endif
                    @include('components.input_sample', [
                        'name' => 'name',
                        'value' => $product->name ?? '',
                    ])
                    @include('components.input_sample', [
                        'name' => 'price',
                        'type' => 'number',
                        'value' => $product->price ?? '',
                    ])
                    @include('components.input_sample', [
                        'name' => 'discount',
                        'type' => 'number',
                        'value' => $product->discount ?? '',
                    ])
                    @include('components.input_sample', [
                        'name' => 'create',
                        'type' => 'date',
                        'date'=>\Carbon\Carbon::now()->toDateString(),
                        'value' => $product->create ?? '',
                    ])
                    @include('components.input_image', [
                        'value' => $product->image ?? '',
                    ])

                    @include('components.input', [
                        'name' => 'user_shop_id',
                        'type' => 'hidden',
                        'value' => $user,
                    ])
                    @if (isset($smallimage))
                   
                    @for ($i = 0; $i < count($smallimage); $i++)
                        @include('components.input_image', [
                        'id_div_image' => 'smallimages',
                        'imagename' => 'image small',
                        'name' => 'small_images[]',
                        'value' => $smallimage[$i]->image_link ?? '',
                        'image_small' => 'small_images',
                    ])
                    @endfor
                        
                    @else
                    <button type="button" id="add_btn" class="ml-5 btn btn-primary">Submit</button>
                        @include('components.input_image', [
                        'id_div_image' => 'smallimages',
                        'imagename' => 'image small',
                        'name' => 'small_images[]',
                        'value' => $smallimage ?? '',
                        'image_small' => 'small_images',
                    ])
                    @endif
                    
                    <div class="mb-3"style="color: black">
                        <select class="form-control" name="category_id" id="">
                            @foreach ($categories as $category)
                                <option
                                    @if (isset($product->category_id)) @selected($category->id=$product->category_id) @endif
                                    value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="ml-5 btn btn-primary">Submit</button>
                </form>
            </div>

            <div class="card-footer clearfix">

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.6.1.slim.min.js"
        integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#add_btn').on('click', function() {
                var html = '';
                html += '<div class="mb-3" id="smallimage">';
                html += ' <div class="mb-4" style="color: black; display: flex">';
                html += '<label for="exampleInputEmail1">image small</label>';
                html += '<input type="file" multiple class="form-control" name="small_images[]"';
                html += '</div>';
                html += '<button type="button" id="remove" class="btn btn-danger">Xoa</button>';
                html += '</div>';
                html +='  @error('small_image')';
                html +=' <div class="alert alert-danger">{{ $message }}</div>';
                html +='@enderror';
           
        
                $('#smallimages').append(html);
            })
        })
        $(document).on('click','#remove',function(){
            $(this).parent().remove();
        })
    </script>
@endsection
