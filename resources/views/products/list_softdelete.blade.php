@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Danh sách sản phẩm</h3>
        </div>
        <div class="card">
            <div class="card-body">
              
                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Category</th>
                            <th>Image</th>
                            <th>Create</th>
                            <th style="width:20px;">Restore</th>
                            <th style="width:20px;">Delete</th>
                        </tr>
           
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->discount }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td><img style="height: 50px;width: 50px;" src="/uploads/{{$product->image}}"></td>
                            <td>{{ $product->create }}</td>
                            <td style="width:20px;"><a class="btn btn-primary"
                                    href="{{ route('product.restore', $product->id) }}">Restore</a></td>
                            <td style="width:20px;">
                                <form action="{{ route('product.realDelete', $product->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    <tbody>



                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
