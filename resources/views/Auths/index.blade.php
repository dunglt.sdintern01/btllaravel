@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Danh sách user</h3>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="p-2 bd-highlight">
                    <a href="{{ route('users.create') }}" class="btn btn-success">Add</a>
                </div>
                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone</th>
                        </tr>

                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->phone }}</td>
                        @endforeach

                    <tbody>



                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
