<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>
    <title>Document</title>
</head>

<body>
    <div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh ;">
        <form action="{{ route('logon') }}" class="border shadow p-3 rounded" method="POST" style="width:400px">
            @csrf
            @include('layouts.alert')
            <h1 class="text-center p3">LOGIN</h1>
            <div class="mb-3">
                <label for="username">Username</label>
                <input type="username" class="form-control" id="username" placeholder="Username" name="username">
            </div>
            @error('username')
                <p style="color: red">{{ $message }}</p>
            @enderror

            <div class="mb-3">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Password" name="password">
            </div>
            @error('password')
                <p style="color: red">{{ $message }}</p>
            @enderror
            <div class="mb-3">
                <button type="submit" name="login" class="btn btn-primary col-12">Login</button>
            </div>
            <a class="btn btn-warning col-12" href="{{ route('register') }}">Register</a><br>
            <a href="{{ route('forgot.Password') }}">Forgot Password</a>
        </form>
    </div>
</body>

</html>
