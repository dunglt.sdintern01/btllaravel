@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body" style="color: black">
        <div class="card-header">
            <h3 style="color: blue" class="card-title">Create user</h3>
        </div>
        <div class="card">

            <div class="card-body">
                <form action="
                    {{ route('add.user') }}" method="GET">
                    @csrf

                    @include('components.input_sample', [
                        'name' => 'name',
                        
                    ])
                    @include('components.input_sample', [
                        'name' => 'email',
                        'type' => 'text',
                        
                    ])
                    @include('components.input_sample', [
                        'name' => 'password',
                        'type' => 'password',
                       
                    ])
                    @include('components.input_sample', [
                        'name' => 'confirmpassword',
                        'type' => 'password',
                       
                    ])
                    @include('components.input_sample', [
                        'name' => 'username',
                        'type' => 'text',
                       
                    ])
                    @include('components.input_sample', [
                        'name' => 'address',
                        'type' => 'text', 
                    ])
                   
                    @include('components.input_sample', [
                        'name' => 'phone',
                        'type' => 'text', 
                    ])
                    @include('components.input', [
                        'name' => 'role',
                        'type' => 'hidden', 
                        'attr'=>'readonly',
                        'value'=>'1'
                    ])
                    
                   
                    <button type="submit" class="ml-5 btn btn-primary">Submit</button>
                </form>
            </div>

            <div class="card-footer clearfix">

            </div>
        </div>
    </div>
@endsection
