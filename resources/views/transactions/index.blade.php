@extends('layouts.view_customer')
@section('content')
    <section class="delivery">
        <div class="container">
            <div class="delivery-top-wrap">
                <div class="delivery-top">
                    <div class="delivery-top-cart delivery-top-item">
                        <i class="fa fa-shopping-bag "></i>
                    </div>
                    <div class="delivery-top-address delivery-top-item">
                        <i class="fa fa-map-marked-alt "></i>
                    </div>
                    {{-- <div class="delivery-top-payment delivery-top-item">
                <i class="fa fa-money-check-alt "></i>
            </div> --}}
                </div>
            </div>

        </div>
        <div class="container">
            <div class="delivery-content row">
                <div class="delivery-content-left">
                    <p>Vui lòng chọn địa chỉ giao hàng</p>
                    <form action="{{route('payment')}}" method="POST">
                        @csrf
                   
                    <div class="delivery-content-left-input-top row">
                        <div class="delivery-content-left-input-top-item">
                            <label for="">Họ tên <span style="color: red ;">*</span></label>
                            <input name="name" value="{{auth()->user()->name ?? ''}}" type="text">
                        </div>
                        <div class="delivery-content-left-input-top-item">
                            <label for="">Điện thoại <span style="color: red ;">*</span></label>
                            <input name="phone" value="{{auth()->user()->phone ?? ''}}" type="text">
                        </div>
                    </div>
                    <div class="delivery-content-left-input-bottom">
                        <label for="">Địa chỉ <span style="color: red ;">*</span></label>
                        <input type="text" value="{{auth()->user()->address ?? ''}}" name="address">
                    </div>
                     
                        <input type="hidden" value="{{auth()->user()->id ?? ''}}" name="user_id">
                   
                    <div class="delivery-content-left-button row">
                        <a href="{{ route('cart.index') }}"><span>&#171;</span>
                            <p>Quay lại giỏ hàng</p>
                        </a>
                        <button type="submit" name="redirect">
                            <p style="font-weight: bold;" > Thanh toán và giao hàng</p>
                        </button>
                    </div>
                </div>
                <div class="delivery-content-right">
                    @php
                        $discount=0;
                        $subtotal = 0;
                        $total = 0;
                    @endphp
                    <table>
                        <tr>
                            <th>Tên sản phẩm</th>
                            <th>Đơn giá </th>
                            <th>Giảm giá </th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                        </tr>
                        @foreach ($carts as $cart)
                            @php
                                $subtotal = (($cart->product_price * $cart->soluongsp) * $cart->discount) / 100;
                                $discount=($cart->product_price * $cart->soluongsp) - $subtotal;
                                $total+=$discount;
                            @endphp
                            <tr>
                                <input hidden type="text" value="{{$cart->id}}" name="cart_id[]">
                                <td>{{ $cart->product_name }}</td>
                                <td>{{ $cart->product_price }}</td>
                                <td>{{ $cart->discount }}%</td>
                                <td>{{ $cart->soluongsp }}</td>
                                <td>
                                    <p>{{number_format($discount)}}<sup>đ</sup></p>
                                </td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="3" style="font-weight: bold;">Tổng</td>
                            <td colspan="3" style="font-weight: bold;">
                                <p>{{number_format( $total)}}<sup>đ</sup></p>
                            </td>
                        </tr>
                        <input type="hidden" name="total" value="{{$total}}">
                    </table>
                </div>
            </div>
        </form>
        </div>
    </section>
@endsection
@section('js')
@endsection
