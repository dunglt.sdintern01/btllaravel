<div class="mb-3" style="color: black">
    <label for="exampleInputEmail1">{{ ucfirst($name) }}</label>
    <input type="{{$type ?? 'text'}}"  class="form-control" name="{{ $name }}"  value="{{ $value ?? ''}}" min="{{$date ?? ''}}">
    @error("$name")
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
