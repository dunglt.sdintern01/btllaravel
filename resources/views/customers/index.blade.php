@extends('layouts.view_customer')
@section('content')
    <section class="banner__ads__section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner__ads__main">
                        <div class="banner__ads__wrapper">
                            <div class="banner__ads__single__item">
                                <span class="banner__ads__subtitle">Weekend Discount 20%</span>
                                <h5 class="banner__ads__title">
                                    Strawberry Fresh <br />
                                    Ice-Cream
                                </h5>
                                <div class="shop__btn">
                                    <a href="product.html" class="btn btn-primary">Shop Now</a>
                                </div>
                            </div>
                            <div class="banner__ads__image">
                                <img src="https://demo.themeies.com/supermart-html/assets/images/banner-ads/ads-01.png"
                                    alt="banner-ads-image" />
                            </div>
                        </div>
                        <div class="banner__ads__wrapper">
                            <div class="banner__ads__single__item">
                                <span class="banner__ads__subtitle">Sale 25% Off</span>
                                <h5 class="banner__ads__title">
                                    Everyday Fresh & <br />
                                    Clean Products
                                </h5>
                                <div class="shop__btn">
                                    <a href="product.html" class="btn btn-primary btn-green">Shop now</a>
                                </div>
                            </div>
                            <div class="banner__ads__image">
                                <img src="https://demo.themeies.com/supermart-html/assets/images/banner-ads/ads-02.png"
                                    alt="banner-ads-image" />
                            </div>
                        </div>
                        <div class="banner__ads__wrapper">
                            <div class="banner__ads__single__item">
                                <span class="banner__ads__subtitle">Sale 25% Off</span>
                                <h5 class="banner__ads__title">
                                    Everyday Fresh & <br />
                                    Clean Products
                                </h5>
                                <div class="shop__btn">
                                    <a href="product.html" class="btn btn-primary btn-green">Shop now</a>
                                </div>
                            </div>
                            <div class="banner__ads__image">
                                <img src="https://demo.themeies.com/supermart-html/assets/images/banner-ads/ads-02.png"
                                    alt="banner-ads-image" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="category__section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Category</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="category__wrapper swiper__pagination">
                        <div class="swiper categorySwiper">
                            <div class="swiper-wrapper">
                                @foreach ($categories as $category)
                                    <div class="swiper-slide">
                                        <a href="{{ route('listproduct.show', $category->id) }}" class="category-card">
                                            <div class="category-card__image">
                                                <img src="/uploads/{{ $category->image }}" alt="icon" />
                                            </div>
                                            <div class="category-card__title">
                                                <h5>{{ $category->name }}</h5>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <section class="product__feature__section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <div class="tabs__wrapper tabs__wrapper__v2">
                            <div class="tabs__filter text-center">
                                <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="best-tab" data-bs-toggle="tab"
                                            data-bs-target="#best-tab-pane" type="button" role="tab"
                                            aria-controls="best-tab-pane" aria-selected="true">
                                            Best Sell
                                        </button>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($products as $product)
                    @if ($loop->first)
                        <div class="col-lg-4">
                            <div class="product-card mb-5 mb-lg-0">
                                <div class="product__image__wrapper product__image__wrapper__v2">
                                    <a href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}"
                                        class="product__image">
                                        <img src="/uploads/{{ $product->image }}" alt="icon" />
                                    </a>
                                    <div class="badge">{{ $product->discount }}%</div>
                                </div>
                                <div class="product__content product__content__v2">
                                    <div class="product__rating">
                                        <ul>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-regular fa-star"></i></li>
                                        </ul>
                                        <div class="total__rating">(321)</div>
                                    </div>
                                    <div class="product__title">
                                        <h5><a
                                                href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}">{{ $product->name }}</a>
                                        </h5>
                                    </div>
                                    <div class="product__bottom">
                                        <div class="product__price">
                                            {{ $product->price }}
                                            <del>{{ $product->discount }}</del>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-2">
                            <div class="product-card mb-5 mb-lg-0">
                                <div class="product__image__wrapper product__image__wrapper__v2">
                                    <a href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}"
                                        class="product__image">
                                        <img src="/uploads/{{ $product->image }}" alt="icon" />
                                    </a>
                                    <div class="badge">{{ $product->discount }}%</div>
                                </div>
                                <div class="product__content product__content__v2">
                                    <div class="product__rating">
                                        <ul>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-regular fa-star"></i></li>
                                        </ul>
                                        <div class="total__rating">(321)</div>
                                    </div>
                                    <div class="product__title">
                                        <h5><a
                                                href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}">{{ $product->name }}</a>
                                        </h5>
                                    </div>
                                    <div class="product__bottom">
                                        <div class="product__price">
                                            {{ $product->price }}
                                            <del>{{ $product->discount }}</del>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    <!-- Product Feature Section End -->

    <!-- Product Collection Start -->

    <!-- Product Collection End -->

    <!-- Trending Section Start -->
    <section class="trending__section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Tranding</h2>
                        <a href="product.html" class="solid-btn">all <i class="fa-solid fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="product__wrapper swiper__pagination">
                        <div class="swiper trending__Swiper">
                            <div class="swiper-wrapper">
                                @foreach ($products as $product)
                                    <div style="width: 200px;">
                                        <div class="product-card">
                                            <div class="product__image__wrapper">
                                                <a href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}"
                                                    class="product__image">
                                                    <img src="/uploads/{{ $product->image }}" alt="icon" />
                                                </a>
                                                <div class="badge">-5%</div>

                                            </div>
                                            <div class="product__content">
                                                <div class="product__rating">
                                                    <ul>
                                                        <li><i class="fa-solid fa-star"></i></li>
                                                        <li><i class="fa-solid fa-star"></i></li>
                                                        <li><i class="fa-solid fa-star"></i></li>
                                                        <li><i class="fa-solid fa-star"></i></li>
                                                        <li><i class="fa-regular fa-star"></i></li>
                                                    </ul>
                                                    <div class="total__rating">(321)</div>
                                                </div>
                                                <div class="product__title">
                                                    <h5><a
                                                            href="{{ route('listdetailproduct.show', [$product->id, $product->category_id]) }}">{{ $product->name }}</a>
                                                    </h5>
                                                </div>
                                                <div class="product__bottom">
                                                    <div class="product__price">
                                                        ${{ $product->price }}
                                                        <del>${{ $product->discount }}</del>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
