@extends('layouts.view_customer')
@section('content')
<section class="archive-category">
    <div class="container">
       
        <div class="row archive-category__inner">
              
            <div class="product-card__wrapper">
       @foreach ($products as $product)
                <div class="product-card">
                    <div class="product__image__wrapper">
                        <a href="{{route('listdetailproduct.show',[$product->id,$product->category_id])}}" class="product__image">
                            <img src="/uploads/{{$product->image}}" alt="icon" />
                        </a>
                        <div class="badge">{{$product->discount}}%</div>
                        <div class="product__actions">
                            <a href="#" class="action__btn">
                                <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M13.8931 2.07333C13.5526 1.73267 13.1483 1.46243 12.7033 1.27805C12.2584 1.09368 11.7814 0.998779 11.2998 0.998779C10.8181 0.998779 10.3412 1.09368 9.89618 1.27805C9.45121 1.46243 9.04692 1.73267 8.70642 2.07333L7.99975 2.78L7.29309 2.07333C6.60529 1.38553 5.67244 0.999136 4.69975 0.999136C3.72706 0.999136 2.79422 1.38553 2.10642 2.07333C1.41863 2.76112 1.03223 3.69397 1.03223 4.66666C1.03223 5.63935 1.41863 6.5722 2.10642 7.26L2.81309 7.96666L7.99975 13.1533L13.1864 7.96666L13.8931 7.26C14.2337 6.91949 14.504 6.51521 14.6884 6.07023C14.8727 5.62526 14.9676 5.14832 14.9676 4.66666C14.9676 4.185 14.8727 3.70807 14.6884 3.26309C14.504 2.81812 14.2337 2.41383 13.8931 2.07333V2.07333Z"
                                        stroke="#252522" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </a>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#prod__preview" class="action__btn">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.666992 7.99996C0.666992 7.99996 3.33366 2.66663 8.00033 2.66663C12.667 2.66663 15.3337 7.99996 15.3337 7.99996C15.3337 7.99996 12.667 13.3333 8.00033 13.3333C3.33366 13.3333 0.666992 7.99996 0.666992 7.99996Z"
                                        stroke="#252522" stroke-linecap="round" stroke-linejoin="round" />
                                    <path
                                        d="M8 10C9.10457 10 10 9.10457 10 8C10 6.89543 9.10457 6 8 6C6.89543 6 6 6.89543 6 8C6 9.10457 6.89543 10 8 10Z"
                                        stroke="#252522" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </a>
                            <a href="#" class="action__btn">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10 9.33329L13.3333 5.99996L10 2.66663" stroke="#252522"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <path
                                        d="M2.66699 13.3333V8.66667C2.66699 7.95942 2.94794 7.28115 3.44804 6.78105C3.94814 6.28095 4.62641 6 5.33366 6H13.3337"
                                        stroke="#252522" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="product__content">
                        <div class="product__rating">
                            <ul>
                                <li><i class="fa-solid fa-star"></i></li>
                                <li><i class="fa-solid fa-star"></i></li>
                                <li><i class="fa-solid fa-star"></i></li>
                                <li><i class="fa-solid fa-star"></i></li>
                                <li><i class="fa-regular fa-star"></i></li>
                            </ul>
                            <div class="total__rating">(321)</div>
                        </div>
                        <div class="product__title">
                            <h5><a href="{{route('listdetailproduct.show',[$product->id,$product->category_id])}}">{{$product->name}}</a></h5>
                        </div>
                        <div class="product__bottom">
                            <div class="product__price">
                                {{$product->price}}
                                <del>{{$product->discount}}</del>
                            </div>
                            <div class="cart__action__btn">
                                <div class="cart__btn">
                                    <a href="#" class="btn btn-outline">Add to cart</a>
                                </div>
                                <div class="quantity cart__quantity">
                                    <button type="button" class="decressQnt">
                                        <span class="bar"></span>
                                    </button>
                                    <input class="qnttinput" type="number" disabled value="0" min="01" max="100" />
                                    <button type="button" class="incressQnt">
                                        <span class="bar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>
          
        </div>
    </div>
     
</section>
@endsection
